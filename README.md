# HTMLs del Proyecto UX #

**Repositorio que contendra los estilos de las versiones Touch y Botones para el Pry. Experiencias de Uso en ATMs**

#### HTMLs para Botones en:
```
  \botones
```
## Versión 1.2:
### Soporte a Incremento de Línea
1. **12.3.1** : *Oferta IL*
2. **12.3.2** : *Consulta IL*
3. **12.3.3** : *Confirmación del Incremento*
4. **12.3.4** : *Impresión de voucher*
5. **12.3.5** : *Por ahora no*
## Versión 1.3:
### Soporte a Extracash
1. **extracash-1** : *Oferta EC*
2. **extracash-2** : *Selección de Cuotas, ejemplo de 3 cuotas*
3. **extracash_-_1_-_elegir_monto_4_cuotas** : *Selección de Cuotas, ejemplo de 4 cuotas*
4. **extracash-3** : *Selección de Cuenta*
5. **extracash-4-tc** : Terminos y Condiciones
6. **extracash-5** : Confirmación Desembolso
7. **extracash-5-dolares** : Confirmación Desembolso con Tipo de Cambio
8. **extracash-9** : Por ahora no
9. **extracash-sin-oferta** : Oferta con Tarjeta de Crédito

## Publicado ##
https://proyecto-ux.herokuapp.com/