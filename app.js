﻿const PORT = process.env.PORT || 5000
const express = require('express')
const directory = require('serve-index');
const app = express()

app.use(directory(__dirname + '/touch', { 'icons': true, stylesheet:__dirname + '/_directory/site.css' }))

app.use(express.static(__dirname + '/touch'))

app.listen(PORT, () => console.log("Listening on ${PORT}"))