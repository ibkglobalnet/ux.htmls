var K3A = external;
window.onerror = errorHandler;
if (!String.prototype.trim) {  String.prototype.trim = function () {    return this.replace(/^\s+|\s+$/g, '');  };}

function errorHandler(message, url, line) {
    external.Log("Scritpt Error:" + message + "\r\nLine:" + line + "\r\nURL:" + url);
    return true;    // Handled
}

function PageLoad(translate) {
    PageLoadEnable("");
    if (typeof(translate) == 'undefined' || translate == null || translate == true)
        K3A.LocalLanguage.TranslateDocument(window.document);
    
    try {
        var machineId = document.getElementById("tagCashierAssis");
        if (machineId != null) {
            
            // Begin CLPC 12092016 - Cambio de función que retorna Nombre Cajero
                //var machineno = K3A.GetProperty("Hosts.NDCHost.MachineNumber");
                var ATMHelper = K3A.GetHelper("ATMHelper");
                var machineno = ATMHelper.GetATMId();
            // End CLPC 12092016
            
            if (machineno != null) {
                machineId.innerText = K3A.LocalLanguage.Format("tagCashierAssis", "machineno=<" +
                                  machineno + ">");
                      
                      
                //Inicio IISA 26112015 QA BELLTECH
                /* Wilmer, si los ids de Lima empiezan con 3 ceros, por ejemplo: I000XXXX y IB000XXX
                Los ids de provincia empiezan con el código de la tienda principal de dicha provincia por ejemplo: 
                Cusco es Tienda 420, los ids de todo cusco son: I420XXXX o IB420XXX
                Arequipa es Tienda 300, los ids de todo aqp son: I300XXXX o IB300XXX
                Siempre 8 dígitos todo.              */
                var telephoneNumber = document.getElementById("tagTelephoneAssis");
                var phoneNumber = "";
                
                if (machineno.substring(0, 4) == "I000" || machineno.substring(0, 5) == "IB000") {
                    // Es un ATM de Lima.
                    phoneNumber = "311 9000";
                    telephoneNumber.innerText = K3A.LocalLanguage.Format("tagTelephoneAssis", "phoneNumber=<" + phoneNumber + ">"); 
                } else {
                    // Es un ATM de Provincia
                    phoneNumber = "0801-00802 ";
                    telephoneNumber.innerText = K3A.LocalLanguage.Format("tagTelephoneAssis", "phoneNumber=<" + phoneNumber + ">"); 
                }
                //Fin IISA 26112015 QA BELLTECH
                
            }
        }
        //Begin IISA 04112016
        var cardType = (external.GetProperty("CustomerApp.IBCustomer.TransactionSchema")).toUpperCase();                                                             
        if (cardType == 'DEBIT') {
            $("#SalaryAdvanceOption").css({"display": "block"}); //IISA 02052017 Commented. //IISA 280917
            //$("#SalaryAdvanceOption").css({"display": "none"}); // IISA 02052017: To hidde the salary advance option as bank required
        } else {
            $("#SalaryAdvanceOption").css({"display": "none"});
        }
        //End IISA 04112016
		
		//CEAN for touch session 20180718
		if("TOUCH"==external.GetProperty("CustomerApp.Customer.CustomerType")){
			$(".btnCancel").html(
					external.LocalLanguage.Lookup("btnFinishOperation")
			);
			
			$("#btnNoReturnCard").text("No");
			
			$("#btnNoReturnCard").parents("a").css("width","160px");
			
			
			$(".txBlock").html(" ");
			
		} else {
			$(".btnCancel").html(
					external.LocalLanguage.Lookup("btnCancel")
			);
		}
		//END CEAN 20180718
    } catch (e) {}
}

function PageLoadNoFocusHandling(translate) {
    PageLoadEnable2();
    if (typeof(translate) == 'undefined' || translate == null || translate == true)
        K3A.LocalLanguage.TranslateDocument(window.document);
    
    try {
        var machineId = document.getElementById("tagCashierAssis");
        if (machineId != null) {
            // Begin CLPC 12092016 - Cambio de función que retorna Nombre Cajero
                //var machineno = K3A.GetProperty("Hosts.NDCHost.MachineNumber");
                var ATMHelper = K3A.GetHelper("ATMHelper");
                var machineno = ATMHelper.GetATMId();
            // End CLPC 12092016
            if (machineno != null) {
                machineId.innerText = K3A.LocalLanguage.Format("tagCashierAssis", "machineno=<" +
                                  machineno + ">");  
                        
                //Inicio IISA 26112015 QA BELLTECH
                /* Wilmer, si los ids de Lima empiezan con 3 ceros, por ejemplo: I000XXXX y IB000XXX
                Los ids de provincia empiezan con el código de la tienda principal de dicha provincia por ejemplo: 
                Cusco es Tienda 420, los ids de todo cusco son: I420XXXX o IB420XXX
                Arequipa es Tienda 300, los ids de todo aqp son: I300XXXX o IB300XXX
                Siempre 8 dígitos todo.              */
                var telephoneNumber = document.getElementById("tagTelephoneAssis");
                var phoneNumber = "";
                
                if (machineno.substring(0, 4) == "I000" || machineno.substring(0, 5) == "IB000") {
                    // Es un ATM de Lima.
                    phoneNumber = "311 9000";
                    telephoneNumber.innerText = K3A.LocalLanguage.Format("tagTelephoneAssis", "phoneNumber=<" + phoneNumber + ">"); 
                } else {
                    // Es un ATM de Provincia
                    phoneNumber = "0801-00802 ";
                    telephoneNumber.innerText = K3A.LocalLanguage.Format("tagTelephoneAssis", "phoneNumber=<" + phoneNumber + ">"); 
                }
                //Fin IISA 26112015 QA BELLTECH
                
            }
        }
        //Begin IISA 04112016
        var cardType = (external.GetProperty("CustomerApp.IBCustomer.TransactionSchema")).toUpperCase();                                                             
        if (cardType == 'DEBIT') {
            //$("#SalaryAdvanceOption").css({"display": "block"}); //IISA 02052017 Commented.
	     //$("#SalaryAdvanceOption").css({"display": "none"}); // IISA 02052017: To hidde the salary advance option as bank required //Commented 130917
            $("#SalaryAdvanceOption").css({"display": "block"}); //IISA 130917
        } else {
            $("#SalaryAdvanceOption").css({"display": "none"});
        }
        //End IISA 04112016
    } catch (e) {}
}

function PageLoadLite() {
    PageLoadEnable("");
}

function PageUnload() {
}

function PageLoadEnable(enableString) {
    try {
        OnLoad();
    }
    catch (e) {
    }
    FocusHandling();
}

function PageLoadEnable2(enableString) {
    try {
        OnLoad();
    }
    catch (e) {
    }
    FocusHandling2();
}

function FocusHandling() {
    var i;
    var inps = document.getElementsByTagName("input");
    if (inps != null) {
        for (i = 0; i < inps.length; i++) {
            if (inps[i].type.toUpperCase() == "BUTTON")
                inps[i].onfocus = onFocusHandler;
            else if (inps[i].type.toUpperCase() == "TEXT")
                inps[i].onfocus = onFocusHandler;
        }
    }
    inps = document.getElementsByTagName("button");
    if (inps != null) {
        for (i = 0; i < inps.length; i++) {
            inps[i].onfocus = onFocusHandler;
        }
    }
    inps = document.getElementsByTagName("div");
    for (i = 0; i < inps.length; i++) {
        if (typeof inps[i].onselectstart != "undefined")
            inps[i].onselectstart = function () { return false; };
        else
            inps[i].onmousedown = function () { return false; };
        inps[i].style.cursor = "default";
    }
}

function FocusHandling2() {
    var i;
    var inps = document.getElementsByTagName("div");
    for (i = 0; i < inps.length; i++) {
        if (typeof inps[i].onselectstart != "undefined")
            inps[i].onselectstart = function () { return false; };
        else
            inps[i].onmousedown = function () { return false; };
        inps[i].style.cursor = "default";
    }
}

function onFocusHandler() {
    window.event.srcElement.blur();
}

function hasClass(element, name) {
    return new RegExp('(\\s|^)' + name + '(\\s|$)').test(element.className);
}

function addClass(element, cls) {
    if (!hasClass(element, cls))
        element.className += " " + cls;
}

function removeClass(element, cls, input) {
    if (hasClass(element, cls)) {
        var reg = new RegExp('(\\s|^)' + cls + '(\\s|$)');
        element.className = element.className.replace(reg, ' ');
    }
    
    if (typeof (input) != 'undefined' && input != null) {
        if (!inputcomplete) {
            inputcomplete = true;
            K3A.SendInput(input);
        }        
    }
}

function SwitchMenuOption(txn) {
    var txnSelected = K3A.GetProperty("CustomerApp.Customer.TransactionSelected");

    if (K3A.ScratchPad.Contains("ChosenCurrency"))
        K3A.ScratchPad.Remove("ChosenCurrency");   
    if (K3A.ScratchPad.Contains("ScreenBeforeTimeout"))
        K3A.ScratchPad.Remove("ScreenBeforeTimeout");
    if (K3A.ScratchPad.Contains("CurrentInputAmount"))
        K3A.ScratchPad.Remove("CurrentInputAmount");
    if (K3A.ScratchPad.Contains("InvalidAmountAdvice"))
        K3A.ScratchPad.Remove("InvalidAmountAdvice");    
                
    K3A.ScratchPad.Set("currentMenuOption", txn);
    if (txnSelected == "") {
        K3A.SendInput(txn);
    } else {
        K3A.ScratchPad.Set("MoreTxn", true);

        if (txn == "Security") {
            K3A.ScratchPad.Set("GotoSecurityPage", true);
            K3A.SendInput("ANOTHERTXN");
        } else if (txn == "Inquiry") {
            K3A.ScratchPad.Set("GotoSecurityPage", false);
            K3A.SendInput("ANOTHERTXN");
        } else {
            if (txn == "CashWithdrawal") {
                if (K3A.ScratchPad.Contains("CashWithdrawalNotAvailable") && external.ScratchPad.Get("CashWithdrawalNotAvailable") == true)
                    return;
            }
            K3A.ScratchPad.Set("GotoSecurityPage", false);
            K3A.SendInput("ANOTHERTXN(" + txn + ")");
        }
    }    
}

function eventFire(el, etype){  if (el.fireEvent) {    el.fireEvent('on' + etype);  } else {    var evObj = document.createEvent('Events');    evObj.initEvent(etype, true, false);    el.dispatchEvent(evObj);  }}
    
function numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

function formatCommasFromString(n) {
    return n.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

function formatCommasDecimales(n) {
    return n.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,");
}