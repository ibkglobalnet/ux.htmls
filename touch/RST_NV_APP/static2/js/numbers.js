function IsNumber(k) {
    if (k == "0" || k == "1" || k == "2" || k == "3" || k == "4" || 
        k == "5" || k == "6" || k == "7" || k == "8" || k == "9") {
        return true;    
    }
    return false;
}
    
function SetCelNumber(inputNumberList, num) {    
    var lengthInput = inputNumberList.length;
    var nval = 0;
    
    $.each(inputNumberList, function () {
        if ($(this).val() != "") {
            nval++;
        }
    });
    
    //Numeric
    if (!isNaN(num) && (nval < 9) && $.trim(num) != "") {
        //Complete cellphone number
        var cellNumber = CompleteCellNumber(inputNumberList);
        //Cellphone number format rules
        var format = CellNumberFormat(cellNumber + num, false);
        if (!format.status) {
            $(".msgerror").css("color", "#ff0000");
            $(".msgerror").show();
            ////$(".msgerror").html(format.string).fadeIn().delay(4000).fadeOut();
            $(".msgerror").html(format.string);
            //return false;
        } else {
            $(".msgerror").html("");
            $(".msgerror").hide();
        }
        //Cellphone number to input
        for (var i = (lengthInput - nval - 1); i < (lengthInput - 1); i++) {
            $(inputNumberList[i]).val($(inputNumberList[i + 1]).val());
        }
        //Set number        
        $(inputNumberList[lengthInput - 1]).val(num);        
    }
}

function CompleteCellNumber(inputNumberList) {
    //Complete cellphone number
    var cellNumber = "";
    for (var i = 0; i < inputNumberList.length; i++) {
        cellNumber += $.trim($(inputNumberList[i]).val());
    }
    return cellNumber;
}

/*function CellNumberFormat(number, length) {    
    //Format number response
    var msg = {
        status: true,
        string: ""
    };
    //First digit of cellphone number must be 9
    if (number[0] != 9) {
        msg.status = false;
        msg.string = "El número debe comenzar con 9";
	//return msg;
    }
    //No diferent digits
    if (number.length == 9 && /^(.)\1+$/.test(number)) {
        msg.status = false;
        msg.string = "No debe repertirse el mismo número 9 veces";
	//return msg;
    }
    //Length < 9
    if (number.length < 9 && length) {
        msg.status = false;
        msg.string = "Debe ingresar un número de 9 dígitos";
	//return msg;
    }
    return msg;
}*/

function CellNumberFormat(number, length) {    
    //Format number response
    var msg = {
        status: true,
        string: ""
    };
    
    //No diferent digits
    if (number.length == 9 && /^(.)\1+$/.test(number)) {
        msg.status = false;
        msg.string = "Debe ingresar un número válido";
	//return msg;
    }
    //Length < 9
    if (number.length < 9 && length) {
        
        //First digit of cellphone number must be 9
        if (number[0] != 9 && number.length > 0) {
            msg.status = false;
            msg.string = "El número debe comenzar con 9";
        } else {
            msg.status = false;
            msg.string = "Debe ingresar un número de 9 dígitos";
        }
        
        if (number.length == 9 && /^(.)\1+$/.test(number)) {
            msg.status = false;
            msg.string = "Debe ingresar un número válido";
        }
    }
    
    if ((number.length - 1) == 9) {
        //First digit of cellphone number must be 9
        if (number[0] != 9 && number.length > 0) {
            msg.status = false;
            msg.string = "El número debe comenzar con 9";
        } else {
            msg.status = false;
            msg.string = "Debe ingresar un número de 9 dígitos";
        }
    } else {
        if (number[0] != 9 && number.length > 0) {
            msg.status = false;
            msg.string = "El número debe comenzar con 9";
        }
    }
    
    if (msg.status) {
        $(".msgerror").html("");
        $(".msgerror").hide();
    }
    
    return msg;
}

function ClearNumbers(inputNumberList) {
    $.each(inputNumberList, function () {
        $(this).val("");
    });
}

function ClearLastNumber(inputNumberList) {
    for (var i = (inputNumberList.length - 1); i >= 0; i--) {
        $(inputNumberList[i]).val($(inputNumberList[i - 1]).val());
    }
}

function IsCelNumberCorrect(inputNumberList) {
    var number = CompleteCellNumber(inputNumberList);    
    var format = CellNumberFormat(number, true);
    return format;
}


// SET NUMBER TO ONLY ONE INPUT TEXT
function setCelNumberForOneInput(input, num, errorClass) {
    var cellNumber = input.val();
    var inputLength = cellNumber.length;
    
    if (inputLength < 9) {
        var format = CellNumberFormat(cellNumber + num, false);
        if (!format.status) {            
            $("#showError").addClass(errorClass);
            $("#textError").html(format.string);
            return false;
        } else {
            $("#showError").removeClass(errorClass);    
        }
        input.val(cellNumber + num);
    }    
}

function clearLastNumberForOneInput(input, errorClass) {
    var cellNumber = CompleteCellNumberForOneInput(input);
    cellNumber = cellNumber.substring(0, cellNumber.length - 1);
    if (cellNumber == "") {
        $("#showError").removeClass(errorClass);    
    }
    input.val(cellNumber);
}

function IsCelNumberCorrectForOneInput(input) {
    var number = CompleteCellNumberForOneInput(input);
    var format = CellNumberFormat(number, true);
    return format;
}

function CompleteCellNumberForOneInput(input) {
    //Complete cellphone number
    return input.val();
}