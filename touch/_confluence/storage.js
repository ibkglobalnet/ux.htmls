function setmoneda(moneda) {
    localStorage.setItem("moneda", moneda);

}
function getmoneda() {

    return localStorage.getItem("moneda");

}

function setmonto(monto) {
    localStorage.setItem("monto", monto);
}

function getmonto() {
    return localStorage.getItem("monto");
}


$(document).ready(function () {
    if (getmoneda() == null) {
        setmoneda('S/');
        setmonto('80');
    }

    $('li.item-menu a').click(function (event) {
        event.preventDefault();
        switch ($.trim($(this).text())) {
            case 'Retira':
                document.location.href = '../6-retira/6-0-0-retiro-selecciona-moneda-monto.html'
                break;
            case 'Consulta':
                document.location.href = '../5-consulta/5-0-0-consulta-selecciona-tipo.html';
                break;
        }
    });

    console.log(document.location);
    switch (document.location.pathname) {       
        //RETIROS
        case '/6-retira/6-0-0-retiro-selecciona-moneda-monto.html':
           
            switch (document.location.hash) {
                case '#dolar':
                    console.log('moneda dolar');
                    $('.bt-no').click();
                    setmoneda('US$');
                    break;
                case '#otromontosoles':
                    console.log('otro monto soles');
                    $('#otromontosoles').click();
                    setmoneda('S/');
                    break;
                case '#otromontodolar':
                    console.log('moneda dolar');
                    $('#otromontodolar').click();
                    setmoneda('US$');
                    break;
                case '#otromontodolarerror':
                    console.log('moneda dolar');
                    $('#otromontodolarerror').click();
                    setmoneda('US$');
                    break;
                case '#otromontosoleserror':
                    console.log('moneda dolar');
                    $('#otromontosoleserror').click();
                    setmoneda('S7');
                    break;
            }
            $('.botones-si-no a').click(function () {
                if ($(this).text() == 'Soles') {
                    setmoneda('S/');
                } else {
                    setmoneda('US$');
                }
            });

            $('a.b-ss').click(function () {
                var monto = $.trim($(this).text()).split(' ');
                setmonto($.trim(monto[monto.length - 1]));
                document.location.href = '6-1-0-retiro-cuentas-retirar.html';
            });

            break;
        case '/6-retira/6-1-0-retiro-cuentas-retirar.html':
            $('.j-page-text p').html('<strong>Monto a retirar:</strong> ' + getmoneda() + ' ' + getmonto());

            $('.p-main a.bt').click(function (event) {
                if ($(this).attr('href') == '') {
                    event.preventDefault();
                    document.location.href = '../6-retira/6-2-0-retiro-frecuente-guardar-retiro.html';
                }
            });
            $('.fancy-main a.bt').click(function (event) {
                if ($(this).attr('href') == '') {
                    event.preventDefault();
                    document.location.href = '../6-retira/6-2-0-retiro-frecuente-guardar-retiro.html';
                }
            });

            switch (document.location.hash) {
                case '#subcuenta':
                    console.log('subcuenta');
                    $('#subcuenta').click();                    
                    break;
            }
            break;
        case '/6-retira/6-2-0-retiro-frecuente-guardar-retiro.html':
            $('.-caja-dos strong').html(getmoneda() + ' ' + getmonto());
            $('.botones-si-no li').click(function () {
                $('.bt-si').removeClass('active');
                $('.bt-no').removeClass('active');
                $(this).addClass('active');


            });

            $('#btnFinalizar').click(function () {
                event.preventDefault();
                if ($('.active').text() == 'Si') {
                    document.location.href = '6-2-1-retiro-frecuente-monto-retirado.html';
                } else {
                    document.location.href = '6-3-0-retiro-transparencia-imprimir-voucher.html';
                }

            });
            break;
        case '/6-retira/6-2-1-retiro-frecuente-monto-retirado.html':
            $('.-caja-dos strong').html(getmoneda() + ' ' + getmonto());

            $('.btn-large').click(function () {
                event.preventDefault();
                document.location.href = '6-3-0-retiro-transparencia-imprimir-voucher.html';

            });

            break;
        case '/6-retira/6-3-0-retiro-transparencia-imprimir-voucher.html':

            $('a.bt').click(function (event) {
                event.preventDefault();

                if ($.trim($(this).text()) != 'No') {

                    document.location.href = '6-4-1-retiro-monto-retirado.html';
                } else {
                    document.location.href = '6-4-2-retiro-monto-retirado-sin-voucher.html';
                }
            });

            break;
        case '/6-retira/6-4-1-retiro-monto-retirado.html':
        case '/6-retira/6-4-2-retiro-monto-retirado-sin-voucher.html':
            $('.-caja-dos strong').html(getmoneda() + ' ' + getmonto());
            break;

        //CONSULTAS
        case '/5-consulta/5-0-0-consulta-selecciona-tipo.html':
            $('.p-main a.bt').click(function (event) {
                event.preventDefault();
                var name = $.trim($(this).text());
                switch (name) {
                    case 'Saldos':
                        document.location.href = '5-1-0-consulta-saldos-cuentas-consultar.html';
                        break;
                    case 'Movimientos':
                        document.location.href = '5-2-0-consulta-movimientos-cuentas-consultar.html';
                        break;
                }
            });
            break;
        case '/5-consulta/5-1-0-consulta-saldos-cuentas-consultar.html':           

            $('.p-main a.bt').click(function (event) {
                if ($(this).attr('href') == '') {
                    event.preventDefault();
                    document.location.href = '5-1-1-consulta-saldos-transparencia-comision.html';
                }
            });

            $('.fancy-main a.bt').click(function (event) {
                if ($(this).attr('href') == '') {
                    event.preventDefault();
                    document.location.href = '5-1-1-consulta-saldos-transparencia-comision.html';
                }
            });
            break;
        case '/5-consulta/5-1-1-consulta-saldos-transparencia-comision.html':
            $('.btsiguiente').click(function () {
                event.preventDefault();
                document.location.href = '5-1-2-consulta-saldos-operacion-exitosa.html';

            });
            break;
        case '/5-consulta/5-2-0-consulta-movimientos-cuentas-consultar.html':

            switch (document.location.hash) {
                case '#subcuenta':
                    console.log('subcuenta');
                    $('#subcuentas').click();
                    break;

            }

            $('.p-main a.bt').click(function (event) {
                if ($(this).attr('href') == '') {
                    event.preventDefault();
                    document.location.href = '5-2-1-consulta-movimientos-transparencia-comision.html';
                }
            });

            $('.fancy-main a.bt').click(function (event) {
                if ($(this).attr('href') == '') {
                    event.preventDefault();
                    document.location.href = '5-2-1-consulta-movimientos-transparencia-comision.html';
                }
            });
            break;
        case '/5-consulta/5-2-1-consulta-movimientos-transparencia-comision.html':
            $('.btsiguiente').click(function () {
                event.preventDefault();
                document.location.href = '5-2-2-consulta-movimientos-operacion-exitosa.html';

            });
            break;
    }

});