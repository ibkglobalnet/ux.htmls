function settipo(moneda) {
    localStorage.setItem("tipotran", moneda);

}
function gettipo() {
    if (localStorage.getItem("tipotran") == null) {
        settipo(':: Entre mis cuentas');
    }
    return localStorage.getItem("tipotran");

}
$(document).ready(function () {

    switch (document.location.pathname) {
        //RETIROS
        case '/9-transferencias/9-0-0-0-selecciona-donde.html':
           
            $('.ac ul li a').click(function () {
                settipo($(this).text());
                event.preventDefault();
                document.location.href = '9-1-0-0-selecciona-cuenta-acargo.html';
            });
            break;
        case '/9-transferencias/9-1-0-0-selecciona-cuenta-acargo.html':
            $('.g-parrafo1 h3').text(':: ' + gettipo());
            $('#btnVolver').click(function () {
                event.preventDefault();
                document.location.href = '9-0-0-0-selecciona-donde.html';
            });
            break;
    }


});